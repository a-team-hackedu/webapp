# ClassEdu

O ClassEdu é uma plataforma de cursos on-line onde o usuário pode criar cursos do tipo livre ou acadêmicos, ,,cadastrar turmas, gerenciar suas aulas, ver o seu progresso nos cursos que o aluno está cadastrado.

## Nossa Stack
```
    Angular 8.1.3
    Spring Boot 2.1.7
```

## Pré-requisitos
```
    Node.js >= 10.x
```

## Como instalar

```
    git clone git@gitlab.com:a-team-hackedu/webapp.git
    cd pasta_Onde_Foi_Clonado
    npm install
    ng serve
```
