import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TopicsRoutingModule } from './topics-routing.module';
import { TopicComponent } from './topic/topic.component';


@NgModule({
  declarations: [TopicComponent],
  imports: [
    CommonModule,
    TopicsRoutingModule
  ],
  exports: [
    TopicComponent
  ]
})
export class TopicsModule { }
