import { Component, OnInit } from '@angular/core';
import { WebcamInitError, WebcamImage, WebcamUtil } from 'ngx-webcam';
import { Subject, Observable } from 'rxjs';
import { NzModalComponent, NzModalService } from 'ng-zorro-antd';
import { WebcamComponent } from '../webcam/webcam.component';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.less']
})
export class ReportComponent implements OnInit {

  constructor(private modalService: NzModalService) { }

  ngOnInit() {

  }

  handleOpenWebcam() {
    this.modalService.create({
      nzTitle: 'Gravar video',
      nzContent: WebcamComponent,
      nzWidth: 700,
      nzFooter: null
    });
  }
}
