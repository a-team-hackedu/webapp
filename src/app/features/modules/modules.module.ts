import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModulesRoutingModule } from './modules-routing.module';
import { ModuleComponent } from './module/module.component';
import {
  NzButtonModule,
  NzCardModule, NzCollapseModule,
  NzDropDownModule,
  NzEmptyModule,
  NzFormModule,
  NzGridModule,
  NzIconModule,
  NzInputModule, NzModalModule, NzSwitchModule, NzTypographyModule
} from 'ng-zorro-antd';
import { AddAulaModalComponent } from './module/add-aula-modal/add-aula-modal.component';
import { ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';


@NgModule({
  declarations: [ModuleComponent, AddAulaModalComponent],
  imports: [
    CommonModule,
    ModulesRoutingModule,
    NzCardModule,
    NzGridModule,
    NzDropDownModule,
    NzIconModule,
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    NzEmptyModule,
    NzCollapseModule,
    NzTypographyModule,
    NzModalModule,
    ReactiveFormsModule,
    QuillModule.forRoot(),
    NzSwitchModule
  ],
  exports: [
    ModuleComponent
  ],
  entryComponents: [
    AddAulaModalComponent
  ]
})
export class ModulesModule { }
