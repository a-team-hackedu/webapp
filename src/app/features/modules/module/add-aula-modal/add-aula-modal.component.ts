import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd';

@Component({
  selector: 'app-add-aula-modal',
  templateUrl: './add-aula-modal.component.html',
  styleUrls: ['./add-aula-modal.component.less']
})
export class AddAulaModalComponent implements OnInit {

  aulaForm: FormGroup;

  constructor(private fb: FormBuilder, private modal: NzModalRef) {
  }

  setContent(value) {
    this.aulaForm.patchValue({
      descricao: value
    });
  }

  ngOnInit() {
    this.aulaForm = this.fb.group({
      titulo: ['', Validators.required],
      descricao: ['']
    });
  }

  handleSubmit() {
    if (!this.aulaForm.valid) {
      return;
    }

    this.modal.destroy({ ...this.aulaForm.value });
  }

  handleCancel() {
    this.modal.destroy()
  }

}
