import {Component, OnInit} from '@angular/core';
import {NzModalService} from 'ng-zorro-antd';
import {AddAulaModalComponent} from './add-aula-modal/add-aula-modal.component';

@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.less']
})
export class ModuleComponent implements OnInit {

  public aulas: any[] = [];

  constructor(private modalService: NzModalService) {
  }

  ngOnInit() {
  }


  public addAula() {
    this.modalService.create({
      nzTitle: 'Adicionar Aula',
      nzContent: AddAulaModalComponent,
      nzWidth: 800
    }).afterClose.subscribe(it => {
      if (it.titulo) {
        this.aulas.push(it);
      }
    });
  }

}
