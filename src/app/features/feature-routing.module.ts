import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BaseComponent} from '../layouts/base/base.component';
import {ConfigComponent} from './config/config.component';
import {WelcomeComponent} from './welcome/welcome.component';
import {ReportComponent} from './report/report.component';
import {AuthGuard} from '../core/interceptors/auth.guard';
import {UserComponent} from './users/user/user.component';
import {TopicComponent} from './topics/topic/topic.component';
import {ModuleComponent} from './modules/module/module.component';
import {CoursesComponent} from './courses/courses.component';
import {BaseAlunoComponent} from 'src/app/layouts/base-aluno/base-aluno.component';
import {CursosComponent} from 'src/app/features/aluno/cursos/cursos.component';
import {MeusCursosComponent} from 'src/app/features/aluno/meus-cursos/meus-cursos.component';
import {MinhasTurmasComponent} from 'src/app/features/aluno/minhas-turmas/minhas-turmas.component';
import {MinhasNotasComponent} from 'src/app/features/aluno/minhas-notas/minhas-notas.component';
import {DashboardComponent} from './aluno/dashboard/dashboard.component';
import {BaseProfessorComponent} from '../layouts/base-professor/base-professor.component';
import {CursosProfessorComponent} from './professor/cursos/cursos.component';
import {MeusCursosProfessorComponent} from './professor/meus-cursos/meus-cursos.component';
import {MinhasTurmasProfessorComponent} from './professor/minhas-turmas/minhas-turmas.component';
import {MeusAlunosComponent} from './professor/meus-alunos/meus-alunos.component';
import {DashboardProfessorComponent} from './professor/dashboard/dashboard.component';
import {CourseDetailComponent} from './courses/course-detail/course-detail.component';
import {AddAulaComponent} from './courses/add-aula/add-aula.component';


const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/features/welcome'},
  {
    path: '',
    component: BaseComponent,
    canActivateChild: [AuthGuard],
    children: [
      {path: 'welcome', component: WelcomeComponent},
      {path: 'report', component: ReportComponent},
      {path: 'config', component: ConfigComponent},
      {path: 'users', component: UserComponent},
      {path: 'topics', component: TopicComponent},
      {path: 'modules', component: ModuleComponent},
      {path: 'courses', component: CoursesComponent},
      {path: 'courses/:id', component: CourseDetailComponent},
      {path: 'courses/:id/aulas/new', component: AddAulaComponent},
    ]
  },
  {
    path: 'aluno',
    component: BaseAlunoComponent,
    // canActivateChild: [AuthGuard],
    children: [
      {path: '', component: DashboardComponent},
      {path: 'cursos', component: CursosComponent},
      {path: 'meus-cursos', component: MeusCursosComponent},
      {path: 'minhas-turmas', component: MinhasTurmasComponent},
      {path: 'minhas-notas', component: MinhasNotasComponent},
      {path: 'dashboard', component: DashboardComponent},
    ]
  },
  {
    path: 'professor',
    component: BaseProfessorComponent,
    // canActivateChild: [AuthGuard],
    children: [
      {path: 'cursos', component: CursosProfessorComponent},
      {path: 'meus-cursos', component: MeusCursosProfessorComponent},
      {path: 'minhas-turmas', component: MinhasTurmasProfessorComponent},
      {path: 'meus-alunos', component: MeusAlunosComponent},
      {path: 'dashboard', component: DashboardProfessorComponent},
      {path: 'report', component: ReportComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeatureRoutingModule {
}
