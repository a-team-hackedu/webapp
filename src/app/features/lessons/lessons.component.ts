import {Component, OnInit} from '@angular/core';
import {CursoService} from '../../core/services/curso.service';
import {NzModalService} from 'ng-zorro-antd';
import {LessonAddModalComponent} from './lesson-add-modal/lesson-add-modal.component';

@Component({
  selector: 'app-courses',
  templateUrl: './lessons.component.html',
  styleUrls: ['./lessons.component.less']
})
export class LessonsComponent implements OnInit {

  public page = {
    content: [],
    totalElements: 0
  };

  constructor(private cursoService: CursoService, private modalService: NzModalService) {
  }

  ngOnInit() {
    this.cursoService.getCursos().subscribe(res => {
      this.page = {
        ...res
      };
    });
  }

  openCursoModal(editing?) {
    this.modalService.create({
      nzTitle: editing ? 'Editar Curso' : 'Adicionar Curso',
      nzContent: LessonAddModalComponent,
      nzComponentParams: {
        editing
      },
      nzWidth: 500
    });
  }

  addCurso() {
    this.openCursoModal();
  }

  editCurso(curso) {
    this.openCursoModal(curso);
  }

}
