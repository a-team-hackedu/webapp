import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { CursoService } from '../../../core/services/curso.service';

@Component({
  selector: 'app-course-add-modal',
  templateUrl: './lesson-add-modal.component.html',
  styleUrls: ['./lesson-add-modal.component.less']
})
export class LessonAddModalComponent implements OnInit {

  public cursoForm: FormGroup;
  public submiting = false;

  public editing: any = null;

  constructor(private fb: FormBuilder, private modal: NzModalRef, private router: Router, private cursoService: CursoService) {
  }

  ngOnInit() {
    this.cursoForm = this.fb.group({
      nome: ['', Validators.required],
      descricao: ['']
    });

    if (this.editing) {
      this.cursoForm.patchValue({
        nome: this.editing.nome
      });
    }
  }

  handleSubmit() {
    if (!this.cursoForm.valid) {
      return;
    }

    const curso = {
      ...this.cursoForm.value
    };

    this.submiting = true;

    let operation: any;

    if (this.editing) {
      operation = this.cursoService.editCurso(this.editing.id, curso);
    } else {
      operation = this.cursoService.saveCurso(curso);
    }

    operation.subscribe(res => {
      this.submiting = false;
      this.modal.destroy();

      this.router.navigate(['/features/courses', res.id, 'details']);
    });

  }

  handleCancel() {
    this.modal.destroy()
  }


}
