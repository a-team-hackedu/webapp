import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LessonsComponent } from './lessons.component';
import { LessonAddModalComponent } from './lesson-add-modal/lesson-add-modal.component';
import { ReactiveFormsModule } from '@angular/forms';
import {
  NzButtonModule, NzCardModule,
  NzDividerModule,
  NzFormModule,
  NzGridModule,
  NzIconModule,
  NzInputModule,
  NzModalModule,
  NzTableModule
} from 'ng-zorro-antd';
import { LessonDetailComponent } from './lesson-detail/lesson-detail.component';

@NgModule({
  declarations: [LessonsComponent, LessonAddModalComponent, LessonDetailComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    NzTableModule,
    NzModalModule,
    NzGridModule,
    NzIconModule,
    NzDividerModule,
    NzCardModule
  ],
  exports: [
    LessonAddModalComponent
  ],
  entryComponents: [
    LessonAddModalComponent,
  ]
})
export class LessonsModule {
}
