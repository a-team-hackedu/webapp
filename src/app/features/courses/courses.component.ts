import {Component, OnInit} from '@angular/core';
import {CursoService} from '../../core/services/curso.service';
import {NzModalService} from 'ng-zorro-antd';
import {CourseAddModalComponent} from './course-add-modal/course-add-modal.component';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.less']
})
export class CoursesComponent implements OnInit {

  public page = {
    content: [],
    totalElements: 0
  };

  constructor(private cursoService: CursoService, private modalService: NzModalService) {
  }

  ngOnInit() {
    this.cursoService.getCursos().subscribe(res => {
      this.page = {
        ...res
      };
    });
  }

  openCursoModal(editing?) {
    this.modalService.create({
      nzTitle: editing ? 'Editar Curso' : 'Adicionar Curso',
      nzContent: CourseAddModalComponent,
      nzComponentParams: {
        editing
      },
      nzWidth: 500
    });
  }

  addCurso() {
    this.openCursoModal();
  }

  editCurso(curso) {
    this.openCursoModal(curso);
  }

}
