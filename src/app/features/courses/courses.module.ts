import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoursesComponent} from './courses.component';
import {CourseAddModalComponent} from './course-add-modal/course-add-modal.component';
import {ReactiveFormsModule} from '@angular/forms';
import {
  NzButtonModule, NzCardModule,
  NzDividerModule, NzEmptyModule,
  NzFormModule,
  NzGridModule,
  NzIconModule,
  NzInputModule, NzListModule,
  NzModalModule, NzSwitchModule,
  NzTableModule
} from 'ng-zorro-antd';
import {CourseDetailComponent} from './course-detail/course-detail.component';
import {RouterModule} from '@angular/router';
import {AddAulaComponent} from './add-aula/add-aula.component';
import {QuillModule} from 'ngx-quill';
import { AulaDetailsComponent } from './aula-details/aula-details.component';

@NgModule({
  declarations: [CoursesComponent, CourseAddModalComponent, CourseDetailComponent, AddAulaComponent, AulaDetailsComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    NzTableModule,
    NzModalModule,
    NzGridModule,
    NzIconModule,
    NzDividerModule,
    NzCardModule,
    RouterModule,
    NzEmptyModule,
    NzSwitchModule,
    QuillModule,
    NzListModule
  ],
  exports: [
    CourseAddModalComponent,
  ],
  entryComponents: [
    CourseAddModalComponent,
    AulaDetailsComponent
  ]
})
export class CoursesModule {
}
