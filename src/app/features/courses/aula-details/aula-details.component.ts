import {Component, OnInit} from '@angular/core';
import {NzModalRef} from 'ng-zorro-antd';

@Component({
  selector: 'app-aula-details',
  templateUrl: './aula-details.component.html',
  styleUrls: ['./aula-details.component.less']
})
export class AulaDetailsComponent implements OnInit {

  public aula: any;

  constructor(private modalRef: NzModalRef) {
  }

  ngOnInit() {
    this.modalRef.getInstance().nzTitle = this.aula.titulo;
  }

}
