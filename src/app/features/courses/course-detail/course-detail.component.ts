import {Component, OnInit} from '@angular/core';
import {NzModalService} from 'ng-zorro-antd';
import {AddAulaModalComponent} from '../../modules/module/add-aula-modal/add-aula-modal.component';
import {ActivatedRoute, Router} from '@angular/router';
import {CursoService} from '../../../core/services/curso.service';
import {AulaDetailsComponent} from '../aula-details/aula-details.component';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.less']
})
export class CourseDetailComponent implements OnInit {
  public aulas: any[] = [];

  constructor(private modalService: NzModalService, private route: ActivatedRoute, private router: Router, private cursoService: CursoService) {
  }

  ngOnInit() {
    this.cursoService.getAulas(this.route.snapshot.params.id).subscribe(res => {
      this.aulas = res.content;
    });
  }

  public addAula() {
    this.router.navigate(['aulas', 'new']);
  }

  public openAula(aula) {
    this.modalService.create({
      nzContent: AulaDetailsComponent,
      nzWidth: 700,
      nzComponentParams: {
        aula
      },
      nzFooter: null
    });
  }

}
