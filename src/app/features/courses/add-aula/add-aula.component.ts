import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NzModalRef, NzModalService} from 'ng-zorro-antd';
import {WebcamComponent} from '../../webcam/webcam.component';
import {ActivatedRoute, Router} from '@angular/router';
import {CursoService} from '../../../core/services/curso.service';

@Component({
  selector: 'app-add-aula-modal',
  templateUrl: './add-aula.component.html',
  styleUrls: ['./add-aula.component.less']
})
export class AddAulaComponent implements OnInit {

  aulaForm: FormGroup;

  cursoId: number;

  constructor(private fb: FormBuilder, private modalService: NzModalService, private route: ActivatedRoute, private router: Router, private cursoService: CursoService) {
  }

  setContent(value) {
    this.aulaForm.patchValue({
      descricao: value
    });
  }

  ngOnInit() {
    this.aulaForm = this.fb.group({
      titulo: ['', Validators.required],
      descricao: ['']
    });

    this.cursoId = this.route.snapshot.params.id;
  }

  handleSubmit() {
    if (!this.aulaForm.valid) {
      return;
    }

    this.cursoService.saveAula(this.cursoId, {...this.aulaForm.value}).subscribe(res => {
      this.router.navigate(['/features/courses', this.cursoId]);
    });
  }

  handleOpenWebcam() {
    this.modalService.create({
      nzTitle: 'Gravar video',
      nzContent: WebcamComponent,
      nzWidth: 700,
      nzFooter: null
    });
  }

}
