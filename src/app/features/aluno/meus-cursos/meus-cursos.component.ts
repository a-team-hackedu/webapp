import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-meus-cursos',
  templateUrl: './meus-cursos.component.html',
  styleUrls: ['./meus-cursos.component.less']
})
export class MeusCursosComponent implements OnInit {

  myCourses: any[] = [
    { titulo: 'Curso Técnico em Administração', descricao: `O Técnico em Administração é o profissional apto a exercer atividades de apoio que envolvam gestão de recursos humanos, materiais, financeiros, mercadológicos e da informação. Esse profissional pode também atuar na gestão das mais diversas áreas de uma organização, com vistas à obtenção dos melhores níveis de produtividade, qualidade e operacionalidade, adequados ao segmento de atuação e ao cenário mercadológico, visando à competitividade do negócio.`, concluido: 10 },
    { titulo: 'Curso Técnico em Logística', descricao: 'Eu sou um curso de blablabla...', concluido: 30 },
    { titulo: 'Curso Livre de Angular 8', descricao: 'Eu sou um curso de blablabla...', concluido: 40 },
    { titulo: 'Treinamento Empresarial RH', descricao: 'Eu sou um curso de blablabla...', concluido: 55 },
    { titulo: 'Capacitação em NR10', descricao: 'Eu sou um curso de blablabla...', concluido: 60 },

  ];

  constructor() { }

  ngOnInit() {
  }

}
