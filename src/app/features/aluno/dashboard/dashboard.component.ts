import {Component, OnInit} from '@angular/core';
import {AulaService} from '../../../core/services/aula.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit {
  aulas: any[] = [];

  constructor(private aulaService: AulaService) {
  }

  ngOnInit() {
    this.aulaService.getAulas().subscribe(res => {
      this.aulas = res.content;
    });
  }

}
