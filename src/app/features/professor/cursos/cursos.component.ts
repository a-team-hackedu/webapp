import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cursos-professor',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.less']
})
export class CursosProfessorComponent implements OnInit {


  courses: any[] = [
    { titulo: 'Curso Técnico em Administração', descricao: `O Técnico em Administração é o profissional apto a exercer atividades de apoio que envolvam gestão de recursos humanos, materiais, financeiros, mercadológicos e da informação.Esse profissional pode também atuar na gestão das mais diversas áreas de uma organização, com vistas à obtenção dos melhores níveis de produtividade, qualidade e operacionalidade, adequados ao segmento de atuação e ao cenário mercadológico, visando à competitividade do negócio.`, vagas: 10 },
    { titulo: 'Curso Técnico em Logística', descricao: `O curso de Logística forma profissionais especializados em atividades de transporte, armazenamento e distribuição de produtos e mercadorias. É uma área voltada para quem quer trabalhar na indústria, serviço e comércio.`, vagas: 30 },
    { titulo: 'Curso Livre de Angular 8', descricao: `Esse curso tem como objetivo principal ensinar a criação de aplicações web com o poderoso framework Angular 8.Para tornar o aprendizado prático e divertido, você criará 7 projetos ao longo do curso, onde serão ensinados passo a passo na prática, os recursos básicos e intermediários do Angular 8`, vagas: 40 },
    { titulo: 'Treinamento Empresarial RH', descricao: `Devido esse novo cenário é notável que os próprios profissionais de RH precisam estar aptos a conquistar, desenvolver e engajar os talentos contratados. Para que isso aconteça, é essencial que esse setor, tão importante para as empresas, passe por um treinamento de recursos humanos ou por outros formatos de aprendizagem, como o processo de coaching.`, vagas: 55 },
    { titulo: 'Capacitação em NR10', descricao: `O Curso em que se aplica a NR10 tem como objetivo garantir a segurança dos trabalhadores envolvidos em serviços de eletricidade observando as normas técnicas oficiais estabelecidas e aplicando aos conhecimentos necessários e esclarecimento de dúvidas.`, vagas: 60 },

  ];

  constructor() { }

  ngOnInit() {
  }

}
