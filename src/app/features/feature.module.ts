import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeatureRoutingModule } from './feature-routing.module';
import { LayoutModule } from '../layouts/layout.module';
import { NgZorroAntdModule, NzButtonModule, NzFormModule, NzInputModule, NzModalModule } from 'ng-zorro-antd';
import { ConfigComponent } from './config/config.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ReportComponent } from './report/report.component';
import { UsersModule } from './users/users.module';
import { ModulesModule } from './modules/modules.module';
import { TopicsModule } from './topics/topics.module';
import { CursosComponent } from './aluno/cursos/cursos.component';
import { MeusCursosComponent } from './aluno/meus-cursos/meus-cursos.component';
import { MinhasTurmasComponent } from './aluno/minhas-turmas/minhas-turmas.component';
import { MinhasNotasComponent } from './aluno/minhas-notas/minhas-notas.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseAddModalComponent } from './courses/course-add-modal/course-add-modal.component';
import { DashboardComponent } from './aluno/dashboard/dashboard.component';
import { CursosProfessorComponent } from './professor/cursos/cursos.component';
import { DashboardProfessorComponent } from './professor/dashboard/dashboard.component';
import { MeusAlunosComponent } from './professor/meus-alunos/meus-alunos.component';
import { MeusCursosProfessorComponent } from './professor/meus-cursos/meus-cursos.component';
import { MinhasTurmasProfessorComponent } from './professor/minhas-turmas/minhas-turmas.component';
import { WebcamModule } from 'ngx-webcam';
import { FormsModule } from '@angular/forms';
import { WebcamComponent } from './webcam/webcam.component';
import { UserComponent } from './users/user/user.component';
import {CoursesModule} from './courses/courses.module';


@NgModule({
  declarations: [
    ConfigComponent,
    WelcomeComponent,
    ReportComponent,
    CursosComponent,
    MeusCursosComponent,
    MinhasTurmasComponent,
    MinhasNotasComponent,
    DashboardComponent,
    CursosProfessorComponent,
    DashboardProfessorComponent,
    MeusAlunosComponent,
    MeusCursosComponent,
    MeusCursosProfessorComponent,
    MinhasTurmasProfessorComponent,
    WebcamComponent
  ],
  imports: [
    CommonModule,
    FeatureRoutingModule,
    NgZorroAntdModule,
    LayoutModule,
    UsersModule,
    ModulesModule,
    TopicsModule,
    CoursesModule,
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    FormsModule,
    WebcamModule,
    NzModalModule
  ],
  entryComponents: [
    WebcamComponent,
    UserComponent
  ]
})
export class FeatureModule {
}
