import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/interceptors/auth.guard';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/features' },
  {
    path: 'features',
    // canActivateChild: [AuthGuard],
    loadChildren: () => import('./features/feature.module').then(m => m.FeatureModule)
  },
  { path: '**', redirectTo: '/features' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
