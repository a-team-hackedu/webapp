import {Injectable} from '@angular/core';
import {Observable, of, BehaviorSubject} from 'rxjs';

import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';

import {environment} from 'src/environments/environment';

import {UserService} from './user.service';

const IS_LOGGED = 'isLogged';

@Injectable({
  providedIn: 'root'
})
export class LoginService {


  userDataStore = new BehaviorSubject({user: null});
  userLogged: Observable<any> = this.userDataStore.asObservable();

  isLoggedDataStore = new BehaviorSubject(this.hasToken());
  isLogged: Observable<boolean> = this.isLoggedDataStore.asObservable();

  constructor(private httpClient: HttpClient, private userService: UserService) {
    console.log('login service localStorage', localStorage.getItem(IS_LOGGED));
    console.log('login service sessionStorage', sessionStorage.getItem(IS_LOGGED));

    const token = this.hasToken() ? sessionStorage.getItem(IS_LOGGED) : localStorage.getItem(IS_LOGGED);
    if (token) {

      console.log('valorStorage', token);

      const options: HttpHeaders = new HttpHeaders().set('authorization', `Bearer ${token}`);

      this.userService.getUser(options).subscribe(
        (resp: HttpResponse<any>) => {
          this.userDataStore.next({user: {...resp}});
        }
      );

    }
  }

  hasToken(): boolean {
    return !!sessionStorage.getItem(IS_LOGGED) && this.userLogged !== null;
  }

  isLoggedIn(): Observable<boolean> {
    return this.isLoggedDataStore.asObservable();
  }

  login(login: string, password: string, remember: boolean = false): Observable<any> {
    const req = {email: login, password, rememberMe: remember};
    return this.httpClient.post<any>(`${environment.urlBase}/auth`, {...req});
  }

  logout() {
    // remove user from local storage to log user out
    sessionStorage.removeItem(IS_LOGGED);
    localStorage.removeItem(IS_LOGGED);
    this.isLoggedDataStore.next(false);
    // location.reload();

  }
}
