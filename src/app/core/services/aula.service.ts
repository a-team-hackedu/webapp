import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AulaService {

  constructor(private httpClient: HttpClient) {
  }

  getAulas(): Observable<any> {
    return this.httpClient.get<any>(`${environment.urlBase}/api/aulas`);
  }
}
