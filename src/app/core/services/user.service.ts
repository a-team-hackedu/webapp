import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) {
  }

  getUser(options: HttpHeaders): Observable<any> {
    return this.httpClient.get<any>(`${environment.urlBase}/api/users/me`, {headers: options});
  }
}
