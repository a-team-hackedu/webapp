import {Injectable} from '@angular/core';
import Swal, {SweetAlertOptions} from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  constructor() {
  }

  show(message: string, options: SweetAlertOptions = {toast: true, timer: 5000, showConfirmButton: false, type: 'info'}) {
    Swal.fire({
      title: `${message}`,
      ...options
    });
  }

  success(message: string, options: SweetAlertOptions = {toast: true, timer: 5000, showConfirmButton: false, type: 'success'}) {
    Swal.fire({
      title: `${message}`,
      type: 'success',
      showConfirmButton: false,
      timer: 5000,
      position: options.position,
      toast: options.toast
    });
  }

  error(message: string) {
    this.show(message, {
      title: message,
      toast: true,
      timer: 5000,
      position: 'top-end',
      showConfirmButton: false,
      type: 'error'
    });
  }

  question(msg, callback) {
    Swal.fire({
      title: msg,
      showCancelButton: true,
      cancelButtonText: 'Não',
      showConfirmButton: true,
      confirmButtonText: 'Sim',
      type: 'question'
    }).then(resp => {
      if (resp.value) {
        callback();
      }
    });
  }
}
