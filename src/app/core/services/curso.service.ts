import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CursoService {

  constructor(private httpClient: HttpClient) {
  }

  getCursos(): Observable<any> {
    return this.httpClient.get<any>(`${environment.urlBase}/api/cursos`);
  }

  saveCurso(curso: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.urlBase}/api/cursos`, {...curso});
  }

  editCurso(id: number, curso: any): Observable<any> {
    return this.httpClient.patch<any>(`${environment.urlBase}/api/cursos/${id}`, {...curso});
  }

  getAulas(id: number): Observable<any> {
    return this.httpClient.get<any>(`${environment.urlBase}/api/cursos/${id}/aulas`);
  }

  saveAula(id: number, body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.urlBase}/api/cursos/${id}/aulas`, {...body});
  }
}
