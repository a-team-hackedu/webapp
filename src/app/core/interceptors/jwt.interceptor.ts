import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('intercept request http..');
    // add authorization header with jwt token if available
    const token = sessionStorage.getItem('isLogged') || localStorage.getItem('isLogged');

    // request = request.clone({
    //     // headers: request.headers.set('Content-Type', `application/x-www-form-urlencoded`)
    //     headers: request.headers.set('Authorization', ``)
    // });

    request = request.clone({
      headers: request.headers.set('Content-Type', `application/json`)
    });

    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }



    return next.handle(request);
  }
}
