import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {AlertService} from '../services/alert.service';


@Injectable({
  providedIn: 'root'
})
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private alertService: AlertService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      if (err.status === 401) {
        // location.reload();
      }

      const error =  err.error.message  || err.message || err.statusText;
      if (err.error.message) {
        this.alertService.show(err.error.message, { type: 'error', position: 'center'});
      }

      return throwError(err.error || err);
    }));
  }
}
