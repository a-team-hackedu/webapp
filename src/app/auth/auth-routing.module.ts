import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthComponent} from './auth.component';
import {LoginComponent} from '../layouts/login/login.component';


const routes: Routes = [
  {
    path: 'auth', component: LoginComponent, children: [
      {path: '', component: AuthComponent},
      {path: 'logout', component: AuthComponent, data: { logout: true }  }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {
}
