import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../core/services/login.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpHeaders, HttpResponse} from '@angular/common/http';
import {IUserAuth} from './models/user.model';
import {AlertService} from '../core/services/alert.service';
import {UserService} from '../core/services/user.service';


const IS_LOGGED = 'isLogged';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.less']
})
export class AuthComponent implements OnInit {

  validateForm: FormGroup;
  submitted = false;
  returnUrl: string;

  constructor(private fb: FormBuilder,
              private loginService: LoginService,
              private route: ActivatedRoute,
              private router: Router,
              private alertService: AlertService,
              private userService: UserService) {
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });

    // reset login status
    this.loginService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.validateForm.invalid) {
      this.submitted = false;
      return;
    }

    const {username, password, remember} = this.validateForm.value;
    // localStorage.setItem('isLogged', JSON.stringify({username}));

    // authentic user
    this.loginService.login(username, password, remember).subscribe(
      (res: any) => {

        console.log('Response header', res);

        if (res) {
          sessionStorage.setItem(IS_LOGGED, res.idToken);

          if (remember) {
            localStorage.setItem(IS_LOGGED, res.idToken);
          }


        } else {
          this.alertService.error(`Falha ao autenticar ${username}!`);
          this.submitted = false;
        }


        const options: HttpHeaders = new HttpHeaders().set('authorization', `Bearer ${res.idToken}`);

        // get info user authenticated
        this.userService.getUser(options).subscribe(
          (resp: any) => {

            const user: IUserAuth = resp;

            this.loginService.userDataStore.next({user: {...user}});
            const url = this.returnUrl ? this.returnUrl : '/';



            this.submitted = false;

            this.router.navigate(['/features'], {}).then(() => {
              this.alertService.success(`Bem vindo ${username}!`);
            }).catch((err) => console.log(err));


          }
        );


      },
      (err: HttpResponse<any>) => {
        if (err.status === 401) {
          const {message} = err || err.body;
          this.alertService.error(`${message}`);
        } else {
          this.alertService.error(`Falha ao contactar o servidor!\n
                                   Caso persista contate o administrador.`);
        }

        this.submitted = false;
        console.error('error', err);
      }
    );

    console.log('submit', this.validateForm.value);
  }

}
