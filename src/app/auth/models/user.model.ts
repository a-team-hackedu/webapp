export interface IUserAuth {
  version?: number;
  createdAt?: string;
  updatedAt?: string;
  id?: number;
  nome?: string;
  email?: string;
  active?: boolean;
  expired?: boolean;
  locked?: boolean;
  credentialsExpired?: boolean;
  roles?: IRoleAuth[];
  accountNonExpired?: boolean;
  accountNonLocked?: boolean;
  credentialsNonExpired?: boolean;
  username?: string;
  enabled?: boolean;
}


interface IRoleAuth {
  id?: number;
  authority?: string;
}
