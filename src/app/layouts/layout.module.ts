import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { BaseComponent } from './base/base.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BaseAlunoComponent } from './base-aluno/base-aluno.component';
import { BaseProfessorComponent } from './base-professor/base-professor.component';



@NgModule({
  declarations: [
    BaseComponent,
    LoginComponent,
    BaseAlunoComponent,
    BaseProfessorComponent,
  ],
  imports: [
    CommonModule,
    NgZorroAntdModule,
    RouterModule,
    ReactiveFormsModule
  ],
  exports: [
    BaseComponent,
    LoginComponent,
    BaseAlunoComponent,
    BaseProfessorComponent,
  ]
})
export class LayoutModule { }
