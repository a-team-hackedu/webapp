import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-professor',
  templateUrl: './base-professor.component.html',
  styleUrls: ['./base-professor.component.less']
})
export class BaseProfessorComponent implements OnInit {
  isCollapsed = false;
  constructor() { }

  ngOnInit() {
  }

}
