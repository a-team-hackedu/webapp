import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../core/services/login.service';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css']
})
export class BaseComponent implements OnInit {
  isCollapsed = false;
  constructor() { }

  ngOnInit() {
    // console.log(this.loginService.userLogged);
  }

}
